from django.shortcuts import render

from main.forms import SubmitForm
from main.views import generate_view_params
from .models import ThingsToDo
from tours.models import Tour
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render


def things_page(request, things_id):
    things = ThingsToDo.objects.get(id=things_id)
    tours = Tour.objects.filter(things_to_do=things)
    context = {
        'things': things,
        'tours': tours,
        'form': SubmitForm(request.POST)
    }
    context.update(generate_view_params(request))
    return render(request, 'things-to-do.html', context=context)


def all_things_to_do(request):
    all_things_to_do = ThingsToDo.objects.all()

    all_things_to_do_list = all_things_to_do
    paginator = Paginator(all_things_to_do_list, 10)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        things_to_do = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        things_to_do = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        things_to_do = paginator.page(paginator.num_pages)

    context = {
        'things_to_do': things_to_do
    }
    context.update(generate_view_params(request))
    return render(request, 'all_things_to_do.html', context)
