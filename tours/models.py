# coding=utf-8
from __future__ import unicode_literals

from ckeditor.fields import RichTextField
from django.db import models


# Create your models here.


class Category(models.Model):
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категория'

    title = models.CharField(max_length=100, verbose_name='Название категории')
    icon = models.ImageField(upload_to='image/categories')

    def __unicode__(self):
        return self.title


class TourDetails(models.Model):
    class Meta:
        verbose_name = 'Детали тура'
        verbose_name_plural = 'Детали тура'

    tour_feature = models.CharField(max_length=100, verbose_name='Особенности тура', null=True)

    def __unicode__(self):
        return self.tour_feature


class Days(models.Model):
    class Meta:
        verbose_name = 'День тура'
        verbose_name_plural = 'Дни тура'

    days_counter = models.IntegerField(default=1, verbose_name='Количество дней')
    image = models.ImageField(verbose_name='Картинка для описания дня', null=True)
    text = RichTextField(null=True)
    tour = models.ForeignKey('Tour', verbose_name='Tour', null=True)

    def __unicode__(self):
        return str(self.days_counter)


class Tour(models.Model):
    class Meta:
        verbose_name = 'Тур'
        verbose_name_plural = 'Туры'

    image = models.ImageField(upload_to='image/popular_tours', verbose_name='Изображение')
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.TextField(max_length=255, verbose_name='Краткое описание')
    date_start = models.DateField(verbose_name='Дата начала тура')
    date_end = models.DateField(verbose_name='Дата окончания тура')
    category = models.ForeignKey('Category', verbose_name='Категория')
    other_details = models.ManyToManyField('TourDetails', verbose_name='Детали тура', null=True, blank=True)
    pdf_file = models.FileField(upload_to='pdf/tour_describe', null=True, blank=False)
    things_to_do = models.ForeignKey('things_to_do.ThingsToDo', verbose_name='Направление', null=True)
    popular_tour = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title
