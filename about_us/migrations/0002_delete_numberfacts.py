# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-12 11:24
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('about_us', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='NumberFacts',
        ),
    ]
