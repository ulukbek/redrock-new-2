# coding=utf-8
from __future__ import unicode_literals

from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models


# Create your models here.


class MainSlider(models.Model):
    class Meta:
        verbose_name = 'Главный слайдер'
        verbose_name_plural = 'Главный слайдер'

    image = models.ImageField(upload_to='image/main_slider', verbose_name='Изображение')
    title = models.CharField(max_length=255, verbose_name='Наименование', null=True, blank=True)
    describe = models.CharField(max_length=500, verbose_name='Краткое описание', null=True, blank=True)

    def __unicode__(self):
        return self.title


class MainInformation(models.Model):
    class Meta:
        verbose_name = 'Основная информация'
        verbose_name_plural = 'Основная информация'

    image = models.ImageField(upload_to='image/main_info')
    text = RichTextUploadingField(max_length=1000, verbose_name='Краткая информация', )

    def __unicode__(self):
        return self.text


class TravelersPhotos(models.Model):
    class Meta:
        verbose_name = 'Фотография туриста'
        verbose_name_plural = 'Фотографии туристов'

    image = models.ImageField(upload_to='image/travelers_photos', verbose_name='Фотографии туристов')
    title = models.CharField(max_length=255, verbose_name='Название фотографии')

    def __unicode__(self):
        return self.title


class Media(models.Model):
    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'

    image = models.ImageField(upload_to='image/media', verbose_name='Изображение')

    def __unicode__(self):
        return str(self.image)


class FeedBack(models.Model):
    class Meta:
        verbose_name = 'FAQ'
        verbose_name_plural = 'FAQ'

    phone = models.CharField(null=True, blank=True, max_length=100)
    email = models.CharField(null=True, blank=True, max_length=100)
    text = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.email
