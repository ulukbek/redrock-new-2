# coding=utf-8
from __future__ import unicode_literals

from ckeditor.fields import RichTextField
from django.db import models


class Subscribers(models.Model):
    class Meta:
        verbose_name = 'подписчик'
        verbose_name_plural = 'Подписчики'

    email = models.EmailField(verbose_name='Почта')

    def __unicode__(self):
        return self.email


class NewsAndBlog(models.Model):
    class Meta:
        verbose_name = 'Блог'
        verbose_name_plural = 'Блог'

    image = models.ImageField(upload_to='image/blog', verbose_name='Изображение')
    title = models.CharField(max_length=255, verbose_name='Название')
    date = models.DateField(auto_now_add=True, verbose_name='Дата публикации')
    text = RichTextField(null=True)
    slider_images = models.ManyToManyField('main.Media', verbose_name='Изображения для слайдера')

    def __unicode__(self):
        return self.title
