from django import forms
from things_to_do.models import ThingsToDo


class FilterForm(forms.Form):
    check_in = forms.DateField(widget=forms.DateInput(attrs={'id': 'from_day', 'type': 'text'}))
    check_out = forms.DateField(widget=forms.DateInput(attrs={'id': 'until_day', 'type': 'text'}))
    guest_count = forms.IntegerField()
    children_count = forms.IntegerField(widget=forms.NumberInput(attrs={'value': '0', 'disabled': ''}))
    direction = forms.ChoiceField()
    hotel = forms.IntegerField(widget=forms.HiddenInput)
    transport = forms.IntegerField(widget=forms.HiddenInput)
    food = forms.IntegerField(widget=forms.HiddenInput)
    name = forms.CharField(max_length=255, required=True, widget=forms.TextInput(attrs={'placeholder': 'Your name'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'E-mail'}))
    country = forms.CharField(max_length=255,
                              widget=forms.TextInput(attrs={'placeholder': 'Your country', 'id': 'country_select'}))
    phone = forms.CharField(max_length=50,
                            widget=forms.TextInput(attrs={'placeholder': 'Phone', 'class': 'mobile_phone'}))
    text = forms.CharField(max_length=1000, widget=forms.Textarea(attrs={'placeholder': 'Comment'}), required=False)

    def __init__(self, *args, **kwargs):
        super(FilterForm, self).__init__(*args, **kwargs)
        direction_choices = (('', '-----'),)
        for item in ThingsToDo.objects.all():
            direction_choices += ((str(item.id), item.title),)
        self.fields['direction'].choices = direction_choices
