# coding=utf-8
from __future__ import unicode_literals

from django.db import models


# Create your models here.

class TripCategory(models.Model):
    class Meta:
        verbose_name = 'Категория тура'
        verbose_name_plural = 'Категории туров'

    category = models.CharField(max_length=255, verbose_name='Категория тура')

    def __unicode__(self):
        return self.category


class Hotels(models.Model):
    class Meta:
        verbose_name = 'Отель'
        verbose_name_plural = 'Отели'

    image = models.ImageField(upload_to='image/hotels', verbose_name='Фото отеля')
    title = models.CharField(max_length=255, verbose_name='Наименование отеля')
    rating = models.IntegerField(default=1, verbose_name='Количество звезд')
    link = models.URLField(verbose_name='Ссылка на отель', null=True, blank=True)
    category = models.CharField(max_length=255, verbose_name='Категория отеля',
                                choices=(('lux', 'Lux'), ('standard', 'Standard'), ('budget', 'Budget')))
    direction = models.ForeignKey('things_to_do.ThingsToDo', verbose_name='Направление', null=True)

    def __unicode__(self):
        return self.title


class Food(models.Model):
    class Meta:
        verbose_name = 'Тип питания'
        verbose_name_plural = 'Типы питания'

    title = models.CharField(max_length=255, verbose_name='Тип питания')
    category = models.CharField(max_length=255, verbose_name='Категория питания',
                                choices=(('lux', 'Lux'), ('standard', 'Standard'), ('budget', 'Budget')))

    def __unicode__(self):
        return self.title


class Transport(models.Model):
    class Meta:
        verbose_name = 'Тип транспорта'
        verbose_name_plural = 'Типы транстпорта'

    image = models.ImageField(upload_to='image/transport', verbose_name='Изображение транспорта')
    category = models.CharField(max_length=255, verbose_name='Категория транспорта',
                                choices=(('lux', 'Lux'), ('standard', 'Standard'), ('budget', 'Budget')))

    def __unicode__(self):
        return str(self.image)
