import random

from django.http import JsonResponse
from django.shortcuts import render
from django.template import loader

from things_to_do.models import ThingsToDo
from tripplanner.models import *
from .forms import FilterForm


# Create your views here.

def index(request):
    form = FilterForm(request.POST)
    lux_hotels = Hotels.objects.filter(category='lux').all()
    standard_hotels = Hotels.objects.filter(category='standard').all()
    budget_hotels = Hotels.objects.filter(category='budget').all()
    context = {
        'lux': lux_hotels,
        'standard': standard_hotels,
        'budget': budget_hotels,
        'lux_transport': Transport.objects.filter(category='lux').all(),
        'standard_transport': Transport.objects.filter(category='standard').all(),
        'budget_transport': Transport.objects.filter(category='budget').all(),
        'lux_food': Food.objects.filter(category='lux').all(),
        'standard_food': Food.objects.filter(category='standard').all(),
        'budget_food': Food.objects.filter(category='budget').all(),
        'category': ThingsToDo.objects.all(),
        'form': form
    }

    return render(request, 'trip-planner.html', context)


def get_hotels_by_direction(request):
    if request.is_ajax():
        direction = request.POST.get('direction')
        if direction != '':
            lux = Hotels.objects.filter(category='lux', direction=direction)
            standard = Hotels.objects.filter(category='standard', direction=direction)
            budget = Hotels.objects.filter(category='budget', direction=direction)

            template = loader.get_template('partial/_hotel.html')
            lux_context = dict(hotels=lux)
            standard_context = dict(hotels=standard)
            budget_context = dict(hotels=budget)

            return JsonResponse(dict(
                lux=template.render(lux_context, request),
                standard=template.render(standard_context, request),
                budget=template.render(budget_context, request)
            ))
        else:
            lux = Hotels.objects.filter(category='lux')
            standard = Hotels.objects.filter(category='standard')
            budget = Hotels.objects.filter(category='budget')

            template = loader.get_template('partial/_hotel.html')
            lux_context = dict(hotels=lux)
            standard_context = dict(hotels=standard)
            budget_context = dict(hotels=budget)

            return JsonResponse(dict(
                lux=template.render(lux_context, request),
                standard=template.render(standard_context, request),
                budget=template.render(budget_context, request)
            ))


def fixtures(request):
    for i in range(0, 20):
        hotel = Hotels()
        hotel.title = 'Hayatt ' + str(random.randrange(1000, 9999))
        hotel.rating = random.randrange(1, 5)
        hotel.link = 'https://google.com/'
        hotel.category = 'lux'
        hotel.direction = ThingsToDo.objects.get(id=random.randrange(1, 2))
        hotel.save()

    for i in range(0, 20):
        hotel = Hotels()
        hotel.title = 'Hayatt ' + str(random.randrange(1000, 9999))
        hotel.rating = random.randrange(1, 5)
        hotel.link = 'https://google.com/'
        hotel.category = 'standard'
        hotel.direction = ThingsToDo.objects.get(id=random.randrange(1, 2))
        hotel.save()

    for i in range(0, 20):
        hotel = Hotels()
        hotel.title = 'Hayatt ' + str(random.randrange(1000, 9999))
        hotel.rating = random.randrange(1, 5)
        hotel.link = 'https://google.com/'
        hotel.category = 'budget'
        hotel.direction = ThingsToDo.objects.get(id=random.randrange(1, 2))
        hotel.save()

    return JsonResponse(dict(success=True, message='Successfully added fixtures...'))
