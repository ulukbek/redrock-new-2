from django.shortcuts import render


# Create your views here.
from main.views import generate_view_params


def index(request):
    context = {

    }
    context.update(generate_view_params(request))
    return render(request, 'contacts.html', context)
