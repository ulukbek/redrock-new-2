/**
 * Created by Nurs on 15.04.2017.
 */
$(document).ready(function () {
    $('.main_blog_slider').slick({
        slidesToShow: 1,
        arrows: false,
        fade: true,
        asNavFor: '.nav_blog_slider',
        draggable: false
    });
    $('.nav_blog_slider').slick({
        slidesToShow: 4,
        arrows: false,
        asNavFor: '.main_blog_slider',
        slidesToScroll: 1,
        dots: false,
        focusOnSelect: true,
        centerMode: true,
    });
});